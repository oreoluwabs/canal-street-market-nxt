import { useRouter } from "next/router";
import Link from "next/link";
import { AnimatePresence, motion } from "framer-motion";
import { Logo } from "./Icons";

const navLinks = [
  {
    id: "about",
    path: "/",
    label: "About",
    sign: (
      <span style={{ width: 20 }}>
        <Logo />
      </span>
    ),
    bgColor: "#fff",
  },
  {
    id: "food",
    path: "/food",
    label: "food",
    sign: "餐饮",
    bgColor: "#5ea3ec",
  },
  {
    id: "Retail",
    path: "/retail",
    label: "Retail",
    sign: "購物",
    bgColor: "#f64444",
  },
  {
    id: "Community",
    path: "/community",
    label: "Community",
    sign: "文化",
    bgColor: "#ffb400",
  },
];

export const Navigation = ({ children }) => {
  const router = useRouter();
  // console.log(router.asPath);
  return (
    <div className="main-wrapper">
      <nav className="navigation">
        <div className="navigation__links">
          <AnimatePresence>
            {navLinks.map(({ id, path, label, sign, bgColor }) => {
              if (path === router.asPath) {
                if (typeof window !== "undefined") {
                  document.querySelector("body").style.backgroundColor =
                    bgColor;
                }

                return (
                  <motion.div
                    key={id}
                    // layoutId="emo"
                    // animate={{ width: "calc(100% - 180px)" }}
                    // initial={{ width: "calc(100% - 280px)" }}
                    // exit={{ width: "calc(100% - 280px)" }}
                    transition={{ duration: 1 }}
                    className="main-content"
                    style={{ "--bgColor": bgColor, display: "inline-block" }}
                  >
                    <motion.div
                      animate={{ opacity: 1 }}
                      initial={{ opacity: 0 }}
                      exit={{ opacity: 0 }}
                      transition={{ duration: 0.25, delay: 0.25 }}
                    >
                      {children}
                    </motion.div>
                  </motion.div>
                );
              }

              return (
                <motion.div
                  key={id}
                  // layoutId="emo"
                  style={{
                    display: "inline-block",
                    verticalAlign: "top",
                    width: 180 / (navLinks.length - 1),
                  }}
                >
                  <Link href={path} key={id} passHref>
                    <a>
                      <div
                        style={{ "--bgColor": bgColor }}
                        className="navlink-item"
                      >
                        <span className="navlink-item__sign">{sign}</span>
                        <span className="navlink-item__text">{label}</span>
                      </div>
                    </a>
                  </Link>
                </motion.div>
              );
            })}
          </AnimatePresence>
        </div>
      </nav>
    </div>
  );
};
