import Link from "next/link";
import { EmailIcon, FacebookIcon, InstagramIcon, MailIcon } from "./Icons";

export const Footer = () => {
  return (
    <footer className="container footer">
      <section className="footer__main">
        <div className="vendor">
          <h3>Interested in becoming a vendor?</h3>
          <button className="btn">click here</button>
        </div>

        <div className="socials">
          <div className="social__links">
            <Link href="/">
              <a className="social__link">
                <span className="social__icon">
                  <EmailIcon />
                </span>
                <span className="social__label">Email us</span>
              </a>
            </Link>
          </div>
          <div className="social__links">
            <Link href="/">
              <a className="social__link">
                <span className="social__icon">
                  <FacebookIcon />
                </span>
                <span className="social__label">
                  Follow us <br /> on facebook
                </span>
              </a>
            </Link>
          </div>
          <div className="social__links">
            <Link href="/">
              <a className="social__link">
                <span className="social__icon">
                  <InstagramIcon />
                </span>
                <span className="social__label">
                  Follow us <br /> on instagram
                </span>
              </a>
            </Link>
          </div>
        </div>
        <div className="newsletter">
          <span className="newsletter__text">
            Stay up to date with our newsletter
          </span>
          <span className="mobile-only newsletter__mobile__text">
            <span className="social__icon">
              <MailIcon />
            </span>
            <span className="social__label">Get updates in your inbox</span>
          </span>
          <form
            className="form"
            onSubmit={(e) => {
              e.preventDefault();
            }}
          >
            <input
              className="form__input"
              type="email"
              name="email"
              placeholder="Email"
            />
            <button type="submit" className="submit__email" />
          </form>
        </div>
      </section>
      <section className="footer__legal">
        <div className="footer__legal--left">
          <span>Copyright Canal Street Market 2019</span>
          <span className="legal__link">
            <Link href="/">
              <a>Privacy Policy</a>
            </Link>
          </span>
          <span className="legal__link">
            <Link href="/">
              <a>Vendor Login</a>
            </Link>
          </span>
        </div>
        <div className="footer__legal--right">
          <Link href="/">
            <a className="site__by">
              Site by <span className="strike">Zero</span>
            </a>
          </Link>
        </div>
      </section>

      <section className="mobile-only footer__legal--mobile">
        <div className="footer__legal--top">
          <span className="legal__link">
            <Link href="/">
              <a>Privacy Policy</a>
            </Link>
          </span>
          <span className="legal__link">
            <Link href="/">
              <a>Vendor Login</a>
            </Link>
          </span>
        </div>
        <div className="footer__legal--bottom">
          <Link href="/">
            <a className="site__by">
              Site by <span className="strike">Zero</span>
            </a>
          </Link>
          <span>Copyright Canal Street Market 2019</span>
        </div>
      </section>
    </footer>
  );
};
