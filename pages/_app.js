import { Footer } from "../components/Footer";
import { Navigation } from "../components/Navigation";
import "../styles/globals.scss";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Navigation>
        <div>
          <Component {...pageProps} />
        </div>
        <Footer />
      </Navigation>
    </>
  );
}

export default MyApp;
