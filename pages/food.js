import Head from "next/head";
import styles from "../styles/home.module.scss";
import otherstyles from "../styles/other.module.scss";

export default function Home() {
  return (
    <div>
      <Head>
        <title>Food | Canal Street Market</title>
        <meta
          name="description"
          content="Canal Street Market is a carefully curated retail market, food hall &amp; community space open year-round at 265 Canal Street"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        {/* Hero */}
        <section className={`${styles["section"]} ${otherstyles["hero"]}`}>
          <h1 className={otherstyles["hero__text"]}>The Food Hall</h1>
        </section>

        {/* Location */}
        <section className={`${styles["section"]} ${styles["location"]}`}>
          <div className={styles["location__grid"]}>
            <div className={styles["address"]}>
              <h3>265 Canal St. New York, NY</h3>
            </div>
            <div className={styles["address__map"]}></div>
          </div>
        </section>
      </main>
    </div>
  );
}
