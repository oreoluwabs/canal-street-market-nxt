import Head from "next/head";
import Link from "next/link";
import { ChineseEvent, Logo } from "../components/Icons";
import styles from "../styles/home.module.scss";

export default function Home() {
  return (
    <div>
      <Head>
        <title>About | Canal Street Market</title>
        <meta
          name="description"
          content="Canal Street Market is a carefully curated retail market, food hall &amp; community space open year-round at 265 Canal Street"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        {/* Hero */}
        <section className={`${styles["section"]} ${styles["hero"]}`}>
          <Link href="/">
            <a className={styles["logo"]}>
              <Logo />
            </a>
          </Link>
          <h1 className={styles["hero__text"]}>
            Canal Street Market is a carefully curated retail market, food hall
            &amp; community space open year-round at 265 Canal Street.
          </h1>
        </section>

        {/* Media */}
        <section className={styles["media"]}>
          <div className={styles["media__img"]}></div>
        </section>

        {/* Market */}
        <section className={`${styles["section"]} ${styles["columns"]}`}>
          <h2 className={styles["columns__header"]}>A New Kind of Market</h2>

          <div className={styles["columns__grid"]}>
            <div className={styles["column"]}>
              <div
                className={styles["column__image"]}
                style={{
                  backgroundImage:
                    'url("https://images.prismic.io/canalstreetmarket/8f74dfabde9ebd66d0d078ba6cf794c77dc8ac5b_home_page_one.jpg?auto=compress,format")',
                }}
              ></div>
              <p>
                Merging retail, food, art, and culture, Canal Street Market
                highlights top retail and design concepts, restaurants, and
                up-and-coming players in the downtown New York City community.
              </p>
            </div>
            <div className={styles["column"]}>
              <div
                className={styles["column__image"]}
                style={{
                  backgroundImage:
                    'url("https://images.prismic.io/canalstreetmarket/20c7997073b456fda0e6de872d23b6183892bdde_home_page_two.jpg?auto=compress,format")',
                }}
              ></div>
              <p>
                Retail Market (AFRAME Coffee) Hours:
                <br />
                Mon – Fri: 8:00AM - 3:00PM
                <br />
                Sat &amp; Sun: 9:00AM - 3:00PM
              </p>
            </div>
            <div className={styles["column"]}>
              <div
                className={styles["column__image"]}
                style={{
                  backgroundImage:
                    'url("https://images.prismic.io/canalstreetmarket/13a988aff3e9c672350fe8330eb7386c3a85eeb9_home_page_three.jpg?auto=compress,format")',
                }}
              ></div>
              <p>
                Food Hall Hours
                <br />
                Mon – Sat: 11:00AM - 6:00PM
                <br />
                Sun: 11:00AM - 6:00PM
              </p>
            </div>
          </div>
        </section>

        {/* Events */}
        <section className={`${styles["section"]} ${styles["events"]}`}>
          <div className={styles["events__header__wrapper"]}>
            <span>
              <ChineseEvent />
            </span>
            <span>
              <h1 className={styles["events__header"]}>Market Events</h1>
            </span>
            <span>
              <ChineseEvent />
            </span>
          </div>
          <div className={styles["events__row"]}>
            <div className={styles["events__row__item"]}>
              <span>02/07</span>
              <p>
                <Link href="/">
                  <a>
                    New Balance x Paperboy Paris by Greenhouse @ Canal Street
                    Market
                  </a>
                </Link>
              </p>
            </div>
            <div className={styles["events__row__item"]}>
              <span>12/11</span>
              <p>
                <Link href="/">
                  <a>Hack City 12/11</a>
                </Link>
              </p>
            </div>
            <div className={styles["events__row__item"]}>
              <span>07/27</span>

              <p>
                <Link href="/">
                  <a>Taiwanese Wave</a>
                </Link>
              </p>
            </div>
            <Link href="/">
              <a className={`btn--link btn--md ${styles["link"]}`}>see all</a>
            </Link>
          </div>
        </section>

        {/* Location */}
        <section className={`${styles["section"]} ${styles["location"]}`}>
          <div className={styles["location__grid"]}>
            <div className={styles["address"]}>
              <h3>265 Canal St. New York, NY</h3>
            </div>
            <div className={styles["address__map"]}></div>
          </div>
        </section>
      </main>
    </div>
  );
}
